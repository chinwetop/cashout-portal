// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  emailRegex: /\S+@\S+\.\S+/,
  phoneRegex: /^\d{11}$/,

  //Other Variables
  CACHE_SIZE: 1,
  REFRESH_INTERVAL: 1000000,



// Web Service Domain
BASE_URL: 'http://gtweb6.gtbank.com/WEBAPIs/PubEncrypt',
BASE_URL4: 'http://gtweb6.gtbank.com/WEBAPIs/PubEncrypt4',

// Service Directory
AUTH_API: '/GTBAuthenticationService/Api',
GTB_USER: '/GTBAdminUserService/api',
REQ_API: '/GTBRequestService/Api',
REQ_API3: '/GTBRequestService/api3',

// Application variables
CHANNEL: '737CASHOUT',
CHANNEL_SHORTNAME: 'CO',

// tslint:disable-next-line:max-line-length --- main one for Ibank
PUB_737_ENC_KEY: `MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgHyNEEA5l75el+6N6OG9qJ10q4nW
XyK0pnlcyzxrLXXahp7oSS1dH7s8UC1evSk1ssYfVqFCUtHBkkOVhOz1eNRLY4zN
aZvi18lppTblxIk7zx/c20MPdls4j/1RGO1bUdoRZ9iijY4my41DuLmcJ8XvRpAW
Kh8g5jsbyaBpzBA5AgMBAAE=`
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
