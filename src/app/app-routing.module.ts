import { NgModule, InjectionToken } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const externalUrlProvider = new InjectionToken('externalUrlRedirectResolver');

const routes: Routes = [
  { path: '', redirectTo: 'onboarding', pathMatch: 'full' },
  {
    path: 'onboarding',
    loadChildren: () =>
      import('./_pages/onboarding/onboarding.module').then((m) => m.OnboardingModule),
  },
  {
    path: 'portal',
    loadChildren: () =>
      import('./_pages/portal/portal.module').then((m) => m.PortalModule),
  },

  { path: '**', redirectTo: 'onboarding', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
