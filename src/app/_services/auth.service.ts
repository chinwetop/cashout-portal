import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
// public AUTH_URL = environment.BASE_URL + environment.AUTH_API;
AUTH_URL = environment.BASE_URL + environment.AUTH_API;
  constructor() { }
}
