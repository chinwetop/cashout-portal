import { Injectable } from '@angular/core';
import * as JsEncryptModule from 'jsencrypt';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EncryptionService {
  JsEncrypt = new JsEncryptModule.JSEncrypt();

  constructor() { }
  randomNumber: any;
  today = Date.now();

  generateNumber() {
    this.randomNumber = null;
    this.randomNumber =
      environment.CHANNEL_SHORTNAME +
       this.today +
      Math.floor(Math.random() * (999999999 - 10000000 + 1) + 10000000);
    return this.randomNumber;
  }

  /**
   * This method is used to encrypt data sent via http request
   * @param data the raw data to be encrypted
   */
  extEncrypt(data: any): string {
    // console.log(data);
    this.JsEncrypt.setPublicKey(environment.PUB_737_ENC_KEY);
    const hash: string = this.JsEncrypt.encrypt(data);
    if (hash && hash.endsWith('==')) {
      return this.extEncrypt(data);
    } else {
      return hash;
    }
  }
}
