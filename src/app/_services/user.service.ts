import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map, retry, catchError, shareReplay } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { UtilitiesService } from './utilities.service';
import { ResponseObject, LoginDetails } from './../_models/user';
import { EncryptionService } from './encryption.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  AUTH_URL = environment.BASE_URL + environment.AUTH_API;
  GTB_AUTH_URL = environment.BASE_URL4 + environment.GTB_USER;

  newUser = new BehaviorSubject<boolean>(false);
  newUser$ = this.newUser.asObservable();
  router: any;

  constructor(
    private http: HttpClient,
    private util: UtilitiesService,
    private encrypt: EncryptionService
    ) {}

  login(formValues: LoginDetails): Observable<ResponseObject> {
    const isGTBankStaff = formValues.isGTBankStaff;
    delete formValues.isGTBankStaff;
    console.log(isGTBankStaff)
    if(isGTBankStaff){
      return this.GTBStaffLogin(formValues);
    } else {
      return this.NoneStaffLogin(formValues);
    }
  }

  GTBStaffLogin(body: LoginDetails): Observable<ResponseObject> {
    const URL = this.GTB_AUTH_URL + `/Login/UserLogin`;
    console.log(body);
    return this.http.post<ResponseObject>(URL, body).pipe(
      shareReplay(),
      retry(3),
      catchError((err) => this.util.handleError(err))
      );

  }

  // NoneStaff(body: LoginDetails): Observable<ResponseObject> {
  //   const URL = this.AUTH_URL + `/737UserLogin`;
  //   // let reqBody = {
  //   //   requestId: this.encrypt.extEncrypt(this.encrypt.generateNumber()),
  //   //   channel: 'gthub',
  //   //   username: body.username,
  //   //   password: body.password
  //   // }
  //   console.log(body);
  //   return this.http.post<ResponseObject>(URL, body).pipe(
  //     shareReplay(),
  //     retry(3),
  //     catchError((err) => this.util.handleError(err))
  //   );
  // }

  NoneStaffLogin(body: LoginDetails): Observable<ResponseObject> {
    const URL = this.AUTH_URL + `/737UserLogin`;
    body = { ...this.util.addAuthParams(body)};
    console.log(body);
    return this.http.post<ResponseObject>(URL, body).pipe(
      shareReplay(),
      retry(3),
      catchError((err) => this.util.handleError(err)),
      map(data => data)
    );
  }
}




// if (res.roleId === 1 && res.defaultPassword === 1) {
//   this.newUser.next(true);
// } else {
//   this.router.navigate(['search']);
// }
