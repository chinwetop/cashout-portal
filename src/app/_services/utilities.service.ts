import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { EncryptionService } from './encryption.service';
import { StaffDetails } from './../_models/user';
import { HttpErrorResponse, HttpHeaderResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UtilitiesService {
  user: StaffDetails;
  constructor(private encryptionService: EncryptionService) {}

  addAuthParams(body) {
    // body.userId = this.encryptionService.extEncrypt(this.user.UserID);
    // body.customerNumber = this.encryptionService.extEncrypt(this.user.UserID);
    body.requestId = this.encryptionService.extEncrypt(this.encryptionService.generateNumber());
    body.channel = environment.CHANNEL;
    return body;
  }

  handleError(error?: HttpErrorResponse) {
    let errormessage;
    console.log(error);
    if (error.error instanceof ErrorEvent) {
      errormessage = `Network Error ${
        error.error.message ||
        error.statusText +
          '. Please check your internet connection and try again.'
      }`;
    } else {
      errormessage = this.getServerErrorMessage(error);
    }
    console.error(errormessage);
    return throwError(errormessage);
  }

  getServerErrorMessage(error: HttpErrorResponse): string {
    const message =
      error.error.responseDescription ||
      error.error.message ||
      error.statusText +
        '. Please check your internet connection and try again.';
    switch (error.status) {
      case 404: {
        return `Not Found: ${message}`;
      }
      case 403: {
        return `Access Denied: ${message}`;
      }
      case 500: {
        return `Internal Server Error: ${message}`;
      }
      default: {
        return (
          message ||
          `Opps! An unknown error occurred and we are unable to handle your request. Please check your internet connection and try again.`
        );
      }
    }
  }
}
