import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { CommonModule } from '@angular/common';
import { OnboardingComponent } from './onboarding.component';

const routes: Routes = [
  {
    path: '', component: OnboardingComponent,
    children: [
      { path: '', redirectTo: '/onboarding/login', pathMatch: 'full' },
      {
        path: 'login',
        loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
      },
      {
        path: 'resetPassword',
        loadChildren: () => import('./reset-password/reset-password.module').then(m => m.ResetPasswordModule)
      }
    ]
  }
];




@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class OnboardingRoutingModule { }
