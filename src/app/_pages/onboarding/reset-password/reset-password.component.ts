import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ResetPasswordService } from './service/reset-password.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  resetForm: FormGroup;
  router: any;
  hide = true;


  constructor(
    private fb: FormBuilder,
    private resetPasswordService: ResetPasswordService
  ) { }

  ngOnInit(): void {
    this.createResetPasswordForm();
  }

  createResetPasswordForm(){
    this.resetForm = this.fb.group({
      UserName: [null, Validators.required],
      newPassWord: [null, Validators.required]
    });
  }

  get UserName() {return this.resetForm.get('UserName');}
  get newPassWord() {return this.resetForm.get('newPassWord');}

  onSubmit(){
    this.resetPasswordService.resetPassword(this.resetForm).subscribe(res => {
     res = this.router.navigate(['search']);
    })

  }

}
