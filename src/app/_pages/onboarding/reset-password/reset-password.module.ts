import { ResetPasswordRoutingModule } from './reset-password-routing.module';
import { ResetPasswordComponent } from './reset-password.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialUiModule } from 'src/app/_utilities/material-ui.module';



@NgModule({
  declarations: [ResetPasswordComponent],
  imports: [
    CommonModule,
    MaterialUiModule,
    ReactiveFormsModule,
    ResetPasswordRoutingModule
  ]
})
export class ResetPasswordModule { }
