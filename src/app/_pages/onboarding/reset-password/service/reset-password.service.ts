import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError, retry, map } from 'rxjs/operators';
import { UtilitiesService } from 'src/app/_services/utilities.service';
import { ResponseObject } from 'src/app/_models/user';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  AUTH_URL = environment.BASE_URL + environment.AUTH_API;

  constructor(
    private http: HttpClient,
    private util: UtilitiesService
  ) { }

  resetPassword(form):Observable<any>{
    const URL = this.AUTH_URL + `update737UserLogin`;
    let body: any
    body = this.util.addAuthParams(form);
    // delete body.customerNumber && body.userId && body.sessionId;
    console.log(body);
    return this.http
    .post<ResponseObject>(URL, body)
    .pipe(
      retry(3),
      catchError(err => this.util.handleError(err)),
      map(res => res.responseDescription)
    );
  }
}
