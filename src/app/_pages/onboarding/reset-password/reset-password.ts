export interface ResetPassword {
    SocialMediaInfo?: any;
    UserID: string;
    UserName: string;
    FirstName: string;
    LastName: string;
    MiddleName: string;
    FullName: string;
    Email: string;
    PhoneNumber: string;
    EmployeeID: string;
    Married: boolean;
    JobRole: string;
    DateOfBirth: Date;
    HireDate: Date;
    DateOfBirth_NoYear: string;
    JobTitle: string;
    Department: string;
    Group: string;
    Division: string;
    Sex: string;
    BranchLocation: string;
    Grade: string;
    BranchCode: string;
    TeamCode: string;
    YearsOfBankingExperience: number;
}

export interface RootObject {
    StaffDetails: ResetPassword;
    ResponseCode: string;
    ResponseDescription: string;
}


