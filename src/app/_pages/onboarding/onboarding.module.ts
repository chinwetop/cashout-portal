import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingComponent } from './onboarding.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialUiModule } from 'src/app/_utilities/material-ui.module';
// import { NotificationComponent } from './notification/notification.component';



@NgModule({
  declarations: [
    OnboardingComponent,
    // NotificationComponent
  ],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    MaterialUiModule,
    RouterModule,
    ReactiveFormsModule

  ]
})
export class OnboardingModule { }
