import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent implements OnInit {

  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
  ) { 

    iconRegistry.addSvgIcon(
      'gtb-logo',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/gtb-logo.svg')
    );
  }

  ngOnInit(): void {
  }

  

}
