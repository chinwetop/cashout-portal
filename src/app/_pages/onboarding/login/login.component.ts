import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { EncryptionService } from 'src/app/_services/encryption.service';
import { UserService } from 'src/app/_services/user.service';
import { UtilitiesService } from 'src/app/_services/utilities.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  hide = true;
  // router: any;
  returnUrl: any;
  checked = false;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    public dialog: MatDialog,
    public util: UtilitiesService,
    public encrypt: EncryptionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.createForm();
    // this.userService.newUser$.subscribe((res) => {
    //   if (res) {
    //     this.callresetDialog();
    //   }
    // });
  }

  createForm() {
    this.loginForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      isGTBankStaff: [false, Validators.required],
    });
  }

  get UserName() {
    return this.loginForm.get('username');
  }
  get Password() {
    return this.loginForm.get('password');
  }
  get isGTBankStaff() {
    return this.loginForm.get('isGTBankStaff');
  }
  onSubmit() {
    this.loginForm.markAllAsTouched();
    console.log(this.loginForm.value);
    if (this.loginForm.valid) {
      this.userService.login(this.loginForm.value).subscribe((res) => {
        if (res) {
          this.router.navigate(['/portal']);
        }
        else if (res.roleId === 1) {
          console.log('zzzzzzzzzzzz');
          // this.router.navigate(['/resetPassword']);
        } else {
          console.log('yyyyyyyyyyyyyy');
          // catchError((err) => this.util.handleError(err));
        }
        console.log(res);
      });
    }
  }

  // onSubmit() {
  //   this.loginForm.markAllAsTouched;
  //   if(this.loginForm.valid){
  //     if(this.checked){
  //       this.userService.postStaffDetails(this.loginForm.value);
  //     }else{
  //       const nonStaffRequest = {
  //         requestId: this.encrypt.extEncrypt(this.encrypt.generateNumber()),
  //         channel: 'gthub',
  //         ...this.loginForm.value
  //       }
  //       this.userService.postNoneStaff(nonStaffRequest);
  //     }
  //   }
  // }

  // else{
  //       const nonStaffRequest = {
  //         requestId: this.encrypt.extEncrypt(this.encrypt.generateNumber()),
  //         channel: 'gthub',
  //         ...this.loginForm.value
  //       }
  //       this.userService.postNoneStaff(nonStaffRequest);
  //     }

  resetPassword() {
    // this.callresetDialog();
  }

  staffCheck($event) {
    this.checked = $event.checked;
    this.isGTBankStaff.setValue($event.checked);
    console.log($event.checked);
  }

  // callresetDialog() {
  //   const ResetDialog = this.dialog.open(ResetPasswordComponent, {
  //     minHeight: '50vh',
  //     width: '40%',
  //     // disableClose: true,
  //   });

  //   ResetDialog.afterClosed().subscribe((result) => {
  //     console.log(`closed: ${result}`);
  //   });
  // }
}
