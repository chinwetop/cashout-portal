import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map, catchError, retry, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { UtilitiesService } from 'src/app/_services/utilities.service';
import { Allstation, Allstation1 } from '../_model/station';
// import { GetUssdTran } from '../_model/transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
BASE_URL = environment.BASE_URL4 + environment.REQ_API3;
TRANS_BASE_URL = environment.BASE_URL4 + environment.REQ_API;

constructor(
  private http: HttpClient,
  public util: UtilitiesService
) { }

/**
   *  Get all Station
   *
   */
  getAllStation$ = this.http.get<Allstation[]>(this.BASE_URL + `/GetStations/0`)
    .pipe(
      tap(data => console.log('Stations', JSON.stringify(data))),
      catchError(this.util.handleError)
    )

  /**
   *  Get Station by Code
   *
   */
  getStation$ = this.http.get<Allstation1>(this.BASE_URL + `/GetStations/166681`)
  .pipe(
    tap(data => console.log('Stations', JSON.stringify(data))),
    catchError(this.util.handleError)
  )

  /**
   *  Get all Transactionh
   *
   */
  // getAllTransactions$ = this.http.get<GetUssdTran[]>(this.TRANS_BASE_URL + `/Get737CashoutTrans`)
  // .pipe(
  //   tap(data => console.log('Transactions', JSON.stringify(data))),
  //   catchError(this.util.handleError)
  // )
}
