export interface GetAllTran {
    customerName: string;
    reference: string;
    trans_Status: string;
    trans_date: string;
    amount: string;
}

export interface AllTrans {
    getUssdTrans: GetAllTran[];
    requestId: string;
    responseCode: string;
    responseDescription: string;
    userId?: any;
}



