export interface Allstation {
    saP_Code: string;
    station: string;
    emailAddress: string;
    phoneNumber: string;
    stationStatus: string;
    stationSupervisor: string;
}

export interface Allstation1 {
    saP_Code?: any;
    station?: any;
    emailAddress?: any;
    phoneNumber?: any;
    stationStatus?: any;
    stationSupervisor?: any;
}

export interface AllActiveStation {
    allstation: Allstation[];
    allstation1: Allstation1;
    requestId?: any;
    responseCode: string;
    responseDescription?: any;
    userId?: any;
}