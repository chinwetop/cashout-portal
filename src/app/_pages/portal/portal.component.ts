import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { TransactionService } from './_services/transaction.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { ResponsiveUtilitiesService } from 'src/app/_services/responsive-utilities.service';

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss']
})
export class PortalComponent implements OnInit {
  isHandset$ = this.responsiveUtils.isHandset$;
  opened$ = this.responsiveUtils.opened$;
 

  constructor(
    fb: FormBuilder,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private responsiveUtils: ResponsiveUtilitiesService
    )  { 

    iconRegistry.addSvgIcon(
      'gtb-logo',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/gtb-logo.svg')
    );
    iconRegistry.addSvgIcon(
      'not-found',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/not-found.svg')
    );
    iconRegistry.addSvgIcon(
      'bg-image',
      sanitizer.bypassSecurityTrustResourceUrl('assets/svg/background-image.svg')
    );
  }

  ngOnInit(): void {

  }

}
