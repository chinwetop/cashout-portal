import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
// import { GetUssdTran } from '../_model/transaction';
import { TransactionService } from '../_services/transaction.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  // transactions$: Observable<GetUssdTran[]>;

  constructor(
    private transactionService: TransactionService

  ) { }

  ngOnInit(): void {
    // this.transactions$ = this.transactionService.getAllTransactions$;
  }

}
