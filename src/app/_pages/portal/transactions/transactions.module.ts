import { ComponentsModule } from './../../../_components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsComponent } from './transactions.component';
import { MaterialUiModule } from 'src/app/_utilities/material-ui.module';



@NgModule({
  declarations: [TransactionsComponent],
  imports: [
    CommonModule,
    MaterialUiModule,
    TransactionsRoutingModule,
    ComponentsModule
  ]
})
export class TransactionsModule { }
