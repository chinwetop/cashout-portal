import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StationsRoutingModule } from './stations-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StationsRoutingModule
  ]
})
export class StationsModule { }
