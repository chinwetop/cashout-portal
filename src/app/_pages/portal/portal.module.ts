import { ComponentsModule } from './../../_components/components.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalRoutingModule } from './portal-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PortalComponent } from './portal.component';
import { MaterialUiModule } from 'src/app/_utilities/material-ui.module';
import { MainToolbarComponent } from 'src/app/_components/_core/main-toolbar/main-toolbar.component';



@NgModule({
  declarations: [PortalComponent],
  imports: [
    CommonModule,
    PortalRoutingModule,
    MaterialUiModule,
    ReactiveFormsModule,
    ComponentsModule
  ]
})
export class PortalModule { }
