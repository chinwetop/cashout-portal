export interface StaffDetails {
  SocialMediaInfo?: any;
  UserID: string;
  UserName: string;
  FirstName: string;
  LastName: string;
  MiddleName: string;
  FullName: string;
  Email: string;
  PhoneNumber: string;
  EmployeeID: string;
  Married: boolean;
  JobRole: string;
  DateOfBirth: Date;
  HireDate: Date;
  DateOfBirth_NoYear: string;
  JobTitle: string;
  Department: string;
  Group: string;
  Division: string;
  Sex: string;
  BranchLocation: string;
  Grade: string;
  BranchCode: string;
  TeamCode: string;
  YearsOfBankingExperience: number;
}


export interface ResponseObject {
  StaffDetails?: StaffDetails;
  roleId?: number;
  defaultPassword?: number;
  stationcode?: number;
  requestId?: string;
  responseCode: string;
  responseDescription: string;
  userId?: any;
}

export interface LoginDetails {
  username: string;
  password: string;
  isGTBankStaff?: boolean;
}
