import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-data-error-handling',
  templateUrl: './data-error-handling.component.html',
  styleUrls: ['./data-error-handling.component.scss']
})
export class DataErrorHandlingComponent implements OnInit {
  @Input() data: any;
  @Input() info: string;
  @Input() error: string;
  @Input() showRetry = true;
  @Input() showBack = true;
  @Input() topMargin = '20vh';
  @Output() retryEvent = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }

  onButtonClick(retry: boolean) {
    this.retryEvent.emit(retry);
  }

}
