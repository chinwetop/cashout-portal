import { TransactDATAService } from './_services/transact-data.service';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';
import { SearchbarService } from '../search-bar/_service/searchbar.service';
import { UtilitiesService } from 'src/app/_services/utilities.service';

@Component({
  selector: 'app-transact-table',
  templateUrl: './transact-table.component.html',
  styleUrls: ['./transact-table.component.scss']
})
export class TransactTableComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['customerName', 'reference', 'trans_Status', 'trans_date', 'amount'];
  isLoading = true;
  isLoadingg = false;
  action!: string;
  dataSource = new MatTableDataSource<any>();
  noData = this.dataSource.connect().pipe(map(data => data.length === 0));
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('table', { static: false }) table: MatTable<any>;
  constructor(public transactionData: TransactDATAService,
    public searchbarService: SearchbarService,
    private util: UtilitiesService
    ) {
     this.getAllTransactions();
   }



  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  retryEventHandler($event){
    console.log($event);
  }

  getAllTransactions() {
    this.transactionData.getAllTransactionData$.subscribe(
      res => {
        console.log(res);
        this.dataSource.data = res;
      });
  }

}
