import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, timer } from 'rxjs';
import { shareReplay, switchMap } from 'rxjs/operators';
import { GetAllTran } from 'src/app/_pages/portal/_model/transaction';
import { environment } from 'src/environments/environment';
import { TransactAPIService } from './transact-api.service';

@Injectable({
  providedIn: 'root'
})
export class TransactDATAService {
  public transactionRequests$: Subject<any> = new BehaviorSubject<any>(null);
  transactionData$ = this.transactionRequests$.asObservable();
  allTransactCache$: Observable<GetAllTran[]>;
  constructor(
    public transactAPI: TransactAPIService
  ) { }

  get allTransactions() {
    if (!this.allTransactCache$ ) {
      const timer$ = timer(0, environment.REFRESH_INTERVAL); // timer that determines the interval before data refresh from server
      this.allTransactCache$ = timer$.pipe(
        switchMap(_ => this.transactAPI.allCashout()),
        shareReplay(environment.CACHE_SIZE)
      );
    }
    return this.allTransactCache$;
  }

  

  getAllTransactionData$ =
  this.transactionData$.pipe(
    switchMap(() => this.allTransactions)
  );


}
