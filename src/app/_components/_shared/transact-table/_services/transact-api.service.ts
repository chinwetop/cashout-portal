import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { AllTrans, GetAllTran } from 'src/app/_pages/portal/_model/transaction';
import { UtilitiesService } from 'src/app/_services/utilities.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactAPIService {

  constructor(
    private http: HttpClient,
    private util: UtilitiesService,
  ) { }

  profileUrl =
  environment.BASE_URL4+
  environment.REQ_API;

  allCashout(): Observable<GetAllTran[]> {
    const PATH = `${this.profileUrl}/Get737CashoutTrans`;
    return this.http.get<AllTrans>(PATH)
      .pipe(
        retry(3),
        catchError(err => this.util.handleError(err)),
        map(data => data.getUssdTrans));
  }

}
