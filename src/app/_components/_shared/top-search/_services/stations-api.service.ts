import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { UtilitiesService } from 'src/app/_services/utilities.service';
import { environment } from 'src/environments/environment';
import { AllActiveStation, Allstation } from '../../../../_pages/portal/_model/station';

@Injectable({
  providedIn: 'root'
})
export class StationsAPIService {

  constructor(
    private http: HttpClient,
    private util: UtilitiesService,
  ) { }

  profileUrl =
  environment.BASE_URL4+
  environment.REQ_API3;

  allStations(): Observable<Allstation[]> {
    const PATH = `${this.profileUrl}/GetActiveStations/0`;
    return this.http.get<AllActiveStation>(PATH)
      .pipe(
        retry(3),
        catchError(err => this.util.handleError(err)),
        map(data => data.allstation));
  }

}
