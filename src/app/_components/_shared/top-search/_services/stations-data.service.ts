import { StationsAPIService } from './stations-api.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, timer } from 'rxjs';
import { shareReplay, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Allstation } from '../../../../_pages/portal/_model/station';

@Injectable({
  providedIn: 'root'
})
export class StationsDATAService {
  public stations$: Subject<any> = new BehaviorSubject<any>(null);
  stationsData$ = this.stations$.asObservable();
  stationsCache$!: Observable<Allstation[]>;
  constructor(public stationAPI: StationsAPIService) { }

  get stations() {
    if (!this.stationsCache$) {
      const timer$ = timer(0, environment.REFRESH_INTERVAL); // timer that determines the interval before data refresh from server
      this.stationsCache$ = timer$.pipe(
        switchMap(_ => this.stationAPI.allStations()),
        shareReplay(environment.CACHE_SIZE)
      );
    }
    return this.stationsCache$;
  }

  
  requestDataState(actionType: string) {
    this.stations$.next(actionType);
  }

  getStations$ = this.stationsData$.pipe(
    switchMap(() => this.stations)
  );

}
