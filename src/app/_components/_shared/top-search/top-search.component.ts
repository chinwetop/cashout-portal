import { StationsDATAService } from './_services/stations-data.service';
import { ResponsiveUtilitiesService } from 'src/app/_services/responsive-utilities.service';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Allstation } from '../../../_pages/portal/_model/station';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-top-search',
  templateUrl: './top-search.component.html',
  styleUrls: ['./top-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopSearchComponent implements OnInit {
  isHandset$ = this.responsiveUtils.isHandset$;
  stations!: Allstation[];
  showStation = true;
  constructor(public responsiveUtils: ResponsiveUtilitiesService, 
    public stationData: StationsDATAService) { }

  ngOnInit(): void {
    this.getStations();
    this.changeStation(1);
  }

  getStations() {
    this.stationData.getStations$.subscribe(res => {
      // this.isLoading = false;
      this.stations = res;
    })
  }

  changeStation($event){
    this.showStation = $event.value == 2 ? true : false;
  }

  selectedStation($event){
    console.log($event);
  }

}
