import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SearchbarService } from './_service/searchbar.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @Input() searchType: any;
  searchListFilter = new FormControl();
  constructor(public searchbarServ: SearchbarService) { }

  ngOnInit(): void {
    this.valueChangeSearchFilter();
  }

  valueChangeSearchFilter() {
    this.searchListFilter.valueChanges.subscribe(res=>{
      this.searchbarServ.searchParam$.next(res);
    })
  }

}
