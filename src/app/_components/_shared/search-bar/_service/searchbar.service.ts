import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchbarService {
  public searchParam$: Subject<string> = new BehaviorSubject<any>(null);
  searchParamData$ = this.searchParam$.asObservable();
  constructor() { }
}
