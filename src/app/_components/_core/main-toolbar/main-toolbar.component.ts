import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResponsiveUtilitiesService } from 'src/app/_services/responsive-utilities.service';

@Component({
  selector: 'app-main-toolbar',
  templateUrl: './main-toolbar.component.html',
  styleUrls: ['./main-toolbar.component.scss']
})
export class MainToolbarComponent implements OnInit {
  isHandset$ = this.responsiveUtils.isHandset$;
  opened$ = this.responsiveUtils.opened$;
  constructor(private responsiveUtils: ResponsiveUtilitiesService, public router: Router) { }

  ngOnInit(): void {
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['/onboarding'])
  }

}
