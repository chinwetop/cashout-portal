import { DataErrorHandlingComponent } from './../data-error-handling/data-error-handling.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopSearchComponent } from './_shared/top-search/top-search.component';
import { MaterialUiModule } from '../_utilities/material-ui.module';
import { MainToolbarComponent } from './_core/main-toolbar/main-toolbar.component';
import { SearchBarComponent } from './_shared/search-bar/search-bar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TransactTableComponent } from './_shared/transact-table/transact-table.component';



@NgModule({
  declarations: [
    TopSearchComponent,
    MainToolbarComponent,
    SearchBarComponent,
    TransactTableComponent,
    DataErrorHandlingComponent
  ],
  imports: [
    CommonModule,
    MaterialUiModule,
    ReactiveFormsModule
  ],
  exports: [
    TopSearchComponent,
    MainToolbarComponent,
    TransactTableComponent,
    SearchBarComponent
  ]
})
export class ComponentsModule { }
